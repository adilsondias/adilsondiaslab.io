 
 document.getElementById("countButton").onclick = function() {    
   
   let quantidadeLetras = {}
   const wordCounts = {}
   const letterCounts = {};
   
   let typedText = document.getElementById("textInput").value;
   typedText = typedText.toLowerCase(); 
   // Isto muda todas as letras para minúsculas
   typedText = typedText.replace(/[^a-z'\s]+/g, ""); 
   // Isso se livra de todos os caracteres exceto letras comuns, espaços e apóstrofos. 
  // Iremos aprender mais sobre como usar a função replace numa lição mais à frente.
  

  for (let i = 0; i < typedText.length; i++) {


   currentLetter = typedText[i];

   if (letterCounts[currentLetter] === undefined) {
      letterCounts[currentLetter] = 1; 
   } else { 
      letterCounts[currentLetter]++; 
   }   
   
}

listWords = typedText.split(' ') 


for (let i = 0; i < listWords.length; i++) {


   currentWord = listWords[i]

   if (wordCounts[currentWord] === undefined) {
      wordCounts[currentWord] = 1; 
   } else { 
      wordCounts[currentWord]++; 
   }
   
   
   console.log(listWords[i])
   
   
}
for (let letter in letterCounts) { 
   const span = document.createElement("span"); 
   const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", "); 
   span.appendChild(textContent); 
   document.getElementById("lettersDiv").appendChild(span); 
}

for (let word in wordCounts) { 
   const span = document.createElement("span"); 
   const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", "); 
   span.appendChild(textContent); 
   document.getElementById("wordsDiv").appendChild(span); 
}



}