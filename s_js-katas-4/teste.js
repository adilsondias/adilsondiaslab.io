
const gotCitiesCSV = "King's Landing, Braavos, Volantis, Old Valyria, Free Cities, Qarth, Meereen"; 

const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";
/*

// formal

// let myArr = ('tiao', 'josé', 'patanal', 123,12.56)


// Literal

let myarr2 = ['teste', 'teste1', 'teste2', 'teste3']

// Associativo

let myarrAssociativo = {
    name: 'Tiago',
    age:18

}




let novoArray = lotrCitiesArray.concat(gotCitiesCSV).concat(bestThing)


// Função every basiciamente percorrer todo o array elemento por elemento.
let verificaEvery = novoArray.every( 

function( elem, ind, obj){

    return ( typeof // tipo de elemento.
         elem == "number")
}   

)

// console.log(lotrCitiesArray);
// console.log(gotCitiesCSV);
// console.log(bestThing);

// console.log(novoArray);

// console.log(verificaEvery);


// ===================================================



function kata1() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 1";
    document.body.appendChild(header); 
    
    var novoArray = []  
    novoArray = gotCitiesCSV.split(', ')
    console.log(novoArray)
    console.log(typeof novoArray)    
    let newElement = document.createElement("div");
    newElement.textContent = novoArray;
    document.body.appendChild(newElement)

    
        
    return gotCitiesCSV; 
}
kata1()



function kata2() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 2";
    document.body.appendChild(header);
    
    

    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(bestThing).split(' ');
    console.log(newElement)
    document.body.appendChild(newElement)      
       
     return bestThing;
} 
kata2();



// Filter

let filterx = lotrCitiesArray.filter(
function verificaElement( elem, ind, obj){
    return ( ind > 3)
}

)

console.log(filterx);




function kata4( ) {
    
    let header = document.createElement("div");
    header.textContent = "Kata 4";
    document.body.appendChild(header);  
    let novoArray = lotrCitiesArray.toString()
     
    let newElement = document.createElement("div");
     newElement.textContent =  novoArray
     document.body.appendChild(newElement)    
     return lotrCitiesArray; 
}

kata4();



function kata5() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 5";
    document.body.appendChild(header);
    novoArray = lotrCitiesArray.filter(

        function(elem, ind, obj){

            return (ind <5)
        }
    )     
      
    
    let newElement = document.createElement("div");
    newElement.textContent = novoArray
    document.body.appendChild(newElement)
    console.log(typeof novoArray)
    console.log(typeof lotrCitiesArray)
        

    return lotrCitiesArray; 
}

kata5()




function kata6( ) {
    
    let header = document.createElement("div");
    header.textContent = "Kata 6";
    document.body.appendChild(header);    
    let novoArray = lotrCitiesArray.filter(
        function(elem, ind, obj){

            return(ind > 3)
        }      

     )

     console.log(novoArray);
     let newElement = document.createElement("div");
     newElement.textContent = novoArray
     document.body.appendChild(newElement)  
    return lotrCitiesArray;
}
kata6()


function kata5() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 5";
    document.body.appendChild(header);
    novoArray = lotrCitiesArray.filter(

        function(elem, ind, obj){

            return (ind  > 3)
        }
    )     
      
    
    let newElement = document.createElement("div");
    newElement.textContent = novoArray
    document.body.appendChild(newElement)
    console.log(typeof novoArray)
    console.log(typeof lotrCitiesArray)
        

    return lotrCitiesArray; 
}

kata5()



function kata6() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 5";
    document.body.appendChild(header);
    let novoArray 
    novoArray = lotrCitiesArray.filter(

        function(elem, ind, obj){
           
            let itemTresCinco
            if (ind == 3 || ind == 5 ) {

                itemTresCinco = elem
                
            }


           
            return itemTresCinco
        }
    )  
    
    console.log(novoArray);
      
    
    let newElement = document.createElement("div");
    newElement.textContent = novoArray
    document.body.appendChild(newElement)
    console.log(typeof novoArray)
    console.log(typeof lotrCitiesArray)
        

    return lotrCitiesArray; 
}

kata6()

// talves fosse possível remover percorrendo com forEach se for entratdo dá um slice no array.
// Assim como na fução filtar na forEach eu também posso fitlar.

// katas 8 
// Para esse exercio poderia uar o delete para remover ou pop o shift se fosse no inicio ou 
// final desse array.

function kata8() {
    let header = document.createElement("div");
    header.textContent = "Kata 8";
    document.body.appendChild(header);

    let rohanI = lotrCitiesArray.indexOf('Rohan')
    console.log(rohanI)
    lotrCitiesArray.splice(rohanI,1) 
   
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)    
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata8()

function kata9() {
    let header = document.createElement("div");
    header.textContent = "Kata 9";
    document.body.appendChild(header);

    let removeItem = lotrCitiesArray.indexOf('Dead Marshes')
    console.log(removeItem)
    lotrCitiesArray.splice(removeItem) 
   
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)    
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata9()

function kata10() {
    let header = document.createElement("div");
    header.textContent = "Kata 10";
    document.body.appendChild(header);

    let addItem = lotrCitiesArray.indexOf('Gondor')
    console.log(addItem)
    lotrCitiesArray.splice(addItem,0, 'Rohan') 
   
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)    
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata10()

function kata11() {
    let header = document.createElement("div");
    header.textContent = "Kata 11";
    document.body.appendChild(header);

    let substem = lotrCitiesArray.indexOf('Dead Marshes')
    console.log(substem)
    lotrCitiesArray.splice(substem,1, 'Deadest Marshes') 
   
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)    
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata11()

*/





function kata12() {
    let header = document.createElement("div");
    header.textContent = "Kata 12";
    document.body.appendChild(header);

    let substring = bestThing.slice(0,14)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata12()

function kata13() {
    let header = document.createElement("div");
    header.textContent = "Kata 13";
    document.body.appendChild(header);

    let substring = bestThing.slice(-12)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata13()

function kata14() {
    let header = document.createElement("div");
    header.textContent = "Kata 14";
    document.body.appendChild(header);

    let substring = bestThing.slice(23,38)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata14()


function kata15() {
    let header = document.createElement("div");
    header.textContent = "Kata 15";
    document.body.appendChild(header);

    let substring = bestThing.substring(-12)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata15()

function kata16() {
    let header = document.createElement("div");
    header.textContent = "Kata 16";
    document.body.appendChild(header);

    let substring = bestThing.slice(23,38)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata16()
var ultimaCidade 
ultimaCidade = lotrCitiesArray.pop()
function kata17() {
    let header = document.createElement("div");
    header.textContent = "Kata 17";
    document.body.appendChild(header);   
        
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata17()

function kata18() {
    let header = document.createElement("div");
    header.textContent = "Kata 18";
    document.body.appendChild(header);
    
   
    lotrCitiesArray.push(ultimaCidade)

    console.log(lotrCitiesArray)
    
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata18()

var primeiracidade 


function kata19() {
    let header = document.createElement("div");
    header.textContent = "Kata 19";
    document.body.appendChild(header);
    
    primeiracidade = lotrCitiesArray.shift()
       

    console.log(lotrCitiesArray)
    
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata19()

function kata20() {
    let header = document.createElement("div");
    header.textContent = "Kata 20";
    document.body.appendChild(header);
    
    lotrCitiesArray.unshift(primeiracidade)
       

    console.log(lotrCitiesArray)
    
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata20()









// let sliceX = lotrCitiesArray.slice(2,5)

// let myArray = [1, 2, 3, 4, 5];

// delete(myArray[2]);
// console.log(myArray)

// console.log(sliceX)

// let myArray = [1, 2, 3, 4, 5];
// console.log("Before: " + myArray);
// myArray.splice(2);
// console.log("After splice(2): " + myArray);

// splice(2, 1) example
// myArray = [1, 2, 3, 4, 5];
// console.log("Before: " + myArray);
// myArray.splice(2, 1);
// console.log("After splice(2, 1): " + myArray);

// // splice(2, 2) example
// myArray = [1, 2, 3, 4, 5];
// console.log("Before: " + myArray);
// myArray.splice(2, 2);
// console.log("After splice(2, 2): " + myArray);


