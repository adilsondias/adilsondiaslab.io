const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen"; 

const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";


function kata1() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 1";
    document.body.appendChild(header); 
    
    var novoArray = []  
    novoArray = gotCitiesCSV.split(', ')
       
    let newElement = document.createElement("div");
    newElement.textContent = novoArray;
    document.body.appendChild(newElement)  
        
    return gotCitiesCSV; 
}
kata1()


function kata2() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 2";
    document.body.appendChild(header);
    
    

    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(bestThing).split(' ');
    console.log(newElement)
    document.body.appendChild(newElement)      
       
     return bestThing;
} 
kata2()

function kata3( ) {
    
    let header = document.createElement("div");
    header.textContent = "Kata 3";
    document.body.appendChild(header);     
    const separador = ","  
     let newElement = document.createElement("div");
     newElement.textContent =  JSON.stringify(gotCitiesCSV).split(separador).join(' ; ')
     document.body.appendChild(newElement)        
        
    return gotCitiesCSV; 
}

kata3()

function kata4( ) {
    
    let header = document.createElement("div");
    header.textContent = "Kata 4";
    document.body.appendChild(header);  
    let novoArray = lotrCitiesArray.toString()
     
    let newElement = document.createElement("div");
     newElement.textContent =  novoArray
     document.body.appendChild(newElement)    
     return lotrCitiesArray; 
}

kata4();

function kata5() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 5";
    document.body.appendChild(header);
    novoArray = lotrCitiesArray.filter(

        function(elem, ind, obj){

            return (ind <5)
        }
    )  
       
    let newElement = document.createElement("div");
    newElement.textContent = novoArray
    document.body.appendChild(newElement)
    console.log(typeof novoArray)
    console.log(typeof lotrCitiesArray)
        

    return lotrCitiesArray; 
}

kata5()

function kata6() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 6";
    document.body.appendChild(header);
    novoArray = lotrCitiesArray.filter(

        function(elem, ind, obj){

            return (ind > 2)
        }
    )  
       
    let newElement = document.createElement("div");
    newElement.textContent = novoArray
    document.body.appendChild(newElement)
    console.log(typeof novoArray)
    console.log(typeof lotrCitiesArray)
        

    return lotrCitiesArray; 
}

kata6()

function kata7() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 7";
    document.body.appendChild(header);
    let novoArray 
    novoArray = lotrCitiesArray.filter(

        function(elem, ind, obj){
           
            let itemTresCinco
            if (ind == 3 || ind == 5 ) {

                itemTresCinco = elem
                
            }


           
            return itemTresCinco
        }
    )  
    
    console.log(novoArray);
      
    
    let newElement = document.createElement("div");
    newElement.textContent = novoArray
    document.body.appendChild(newElement)
    console.log(typeof novoArray)
    console.log(typeof lotrCitiesArray)
        

    return lotrCitiesArray; 
}

kata7()

function kata8() {
    let header = document.createElement("div");
    header.textContent = "Kata 8";
    document.body.appendChild(header);

    let rohanI = lotrCitiesArray.indexOf('Rohan')
    console.log(rohanI)
    lotrCitiesArray.splice(rohanI,1) 
   
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)    
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata8()

function kata9() {
    let header = document.createElement("div");
    header.textContent = "Kata 9";
    document.body.appendChild(header);

    let removeItem = lotrCitiesArray.indexOf('Dead Marshes')
    console.log(removeItem)
    lotrCitiesArray.splice(removeItem) 
   
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)    
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata9()


function kata10() {
    let header = document.createElement("div");
    header.textContent = "Kata 10";
    document.body.appendChild(header);

    let addItem = lotrCitiesArray.indexOf('Gondor')
    console.log(addItem)
    lotrCitiesArray.splice(addItem,0, 'Rohan') 
   
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)    
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata10()


function kata11() {
    let header = document.createElement("div");
    header.textContent = "Kata 11";
    document.body.appendChild(header);

    let substem = lotrCitiesArray.indexOf('Dead Marshes')
    console.log(substem)
    lotrCitiesArray.splice(substem,1, 'Deadest Marshes') 
   
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)    
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata11()




function kata12() {
    let header = document.createElement("div");
    header.textContent = "Kata 12";
    document.body.appendChild(header);

    let substring = bestThing.slice(0,14)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata12()

function kata13() {
    let header = document.createElement("div");
    header.textContent = "Kata 13";
    document.body.appendChild(header);

    let substring = bestThing.slice(-12)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata13()

function kata14() {
    let header = document.createElement("div");
    header.textContent = "Kata 14";
    document.body.appendChild(header);

    let substring = bestThing.slice(23,38)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata14()


function kata15() {
    let header = document.createElement("div");
    header.textContent = "Kata 15";
    document.body.appendChild(header);

    let substring = bestThing.substring(-12)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata15()

function kata16() {
    let header = document.createElement("div");
    header.textContent = "Kata 16";
    document.body.appendChild(header);

    let substring = bestThing.slice(23,38)
    console.log(substring)
      let newElement = document.createElement("div");
    newElement.textContent =  substring   
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata16()
var ultimaCidade 
ultimaCidade = lotrCitiesArray.pop()
function kata17() {
    let header = document.createElement("div");
    header.textContent = "Kata 17";
    document.body.appendChild(header);   
        
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata17()

function kata18() {
    let header = document.createElement("div");
    header.textContent = "Kata 18";
    document.body.appendChild(header);
    
   
    lotrCitiesArray.push(ultimaCidade)

    console.log(lotrCitiesArray)
    
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata18()

var primeiracidade 


function kata19() {
    let header = document.createElement("div");
    header.textContent = "Kata 19";
    document.body.appendChild(header);
    
    primeiracidade = lotrCitiesArray.shift()
       

    console.log(lotrCitiesArray)
    
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata19()

function kata20() {
    let header = document.createElement("div");
    header.textContent = "Kata 20";
    document.body.appendChild(header);
    
    lotrCitiesArray.unshift(primeiracidade)
       

    console.log(lotrCitiesArray)
    
    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)
    document.body.appendChild(newElement)  
  
    return lotrCitiesArray; 
}

kata20()




/*

function kata2() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 2";
    document.body.appendChild(header);  

    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(bestThing).split(' ');
    document.body.appendChild(newElement)      
       
     return bestThing;
} 
kata2();

console.log('Kata 2');
console.log(gotCitiesCSV  + '\n');
console.log(lotrCitiesArray);
console.log(bestThing);


function kata3( ) {
    
    let header = document.createElement("div");
    header.textContent = "Kata 3";
    document.body.appendChild(header);     
    const separador = ","  
     let newElement = document.createElement("div");
     newElement.textContent =  JSON.stringify(gotCitiesCSV).split(separador).join(' ; ')
     document.body.appendChild(newElement)        
        
    return gotCitiesCSV; 
}

kata3()
console.log('Kata 3');
console.log(gotCitiesCSV  );
console.log(lotrCitiesArray);
console.log(bestThing);


function kata4( ) {
    
    let header = document.createElement("div");
    header.textContent = "Kata 4";
    document.body.appendChild(header);  
    let novovalor = lotrCitiesArray
    const separador = ","
  
    let newElement = document.createElement("div");
     newElement.textContent =  JSON.stringify(lotrCitiesArray).split(separador).join(' ; ')
     document.body.appendChild(newElement)    
     return gotCitiesCSV; 
}

kata4();

console.log('Kata 4');
console.log(gotCitiesCSV  + '\n');
console.log(lotrCitiesArray);
console.log(bestThing);


function kata5( ) {
    
    let header = document.createElement("div");
    header.textContent = "Kata 5";
    document.body.appendChild(header);
    var novoArray = []
     novoArray = lotrCitiesArray.filter(novoArray => novoArray < 5)
    console.log(novoArray)
    
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(novoArray)
    document.body.appendChild(newElement)
    console.log(typeof novoArray)
    console.log(typeof lotrCitiesArray)
        

    return lotrCitiesArray; 
}

kata5()

console.log('Kata 5');
console.log(gotCitiesCSV  + '\n');
console.log(lotrCitiesArray);
console.log(bestThing);



function kata6( ) {
    
    let header = document.createElement("div");
    header.textContent = "Kata 6";
    document.body.appendChild(header);    
     var novoArray2 = lotrCitiesArray
     let newElement = document.createElement("div");
     newElement.textContent = JSON.stringify(lotrCitiesArray)
     document.body.appendChild(newElement)  
    return lotrCitiesArray;
}
kata6()

function kata8(){

    let header = document.createElement("div");
    header.textContent = "Kata 8";
    document.body.appendChild(header);
    
    let itensRemove = lotrCitiesArray // 
    
    console.log(itensRemove)

    let newElement = document.createElement("div");
    newElement.textContent =  JSON.stringify(lotrCitiesArray)

    return lotrCitiesArray
    
}
kata8()


function kata3b() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 3b";
    document.body.appendChild(header);   
    
   
    

     let newElement = document.createElement("div");
     newElement.textContent =  JSON.stringify(bestThing).split(' ');
     document.body.appendChild(newElement)
        
    
    

    return bestThing; // Don't forget to return your output!
}

kata3b();

function kata6b() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 6b ";
    document.body.appendChild(header); 
    
    const cidade = lotrCitiesArray.filter(cidade => cidade == 'Mirkwood')
    
     let newElement = document.createElement("div");
     if( cidade == 'Mirkwood'){
     newElement.textContent =  'Sim'
     document.body.appendChild(newElement)
        
     }else{
        newElement.textContent =  'Não'
        document.body.appendChild(newElement)  
    }
    

    return lotrCitiesArray // Don't forget to return your output!
}

kata6b();

function kata7b() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 7b";
    document.body.appendChild(header); 
    
    const cidade = lotrCitiesArray.filter(cidade => cidade == 'Hollywood')
    
     let newElement = document.createElement("div");
     if( cidade == 'Hollywood'){
     newElement.textContent =  'Sim'
     document.body.appendChild(newElement)
        
     }else{
        newElement.textContent =  'Não'
        document.body.appendChild(newElement)  
    }
    

    return lotrCitiesArray 
}

kata7b();

function kata8() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 8";
    document.body.appendChild(header);   
    
    let newElement = document.createElement("div");   
    newElement.textContent =  lotrCitiesArray.findIndex(cidade => cidade == 'Mirkwood')
    document.body.appendChild(newElement)     
    

    return lotrCitiesArray 
}

kata8();

function kata10() {
    
    let header = document.createElement("div");
    header.textContent = "Kata 10";
    document.body.appendChild(header);   
    
    invertArray = lotrCitiesArray.reverse()
    let newElement = document.createElement("div");   
    newElement.textContent =  invertArray
    document.body.appendChild(newElement)     
    

    return lotrCitiesArray 
}

kata10();

*/
