//comece a criar a sua função add na linha abaixo

function add(a , b){
 
  let soma = a + b 

 return soma
 
}
// descomente a linha seguinte para testar sua função
console.assert(add(3, 5) === 8, 'A função add não está funcionando como esperado');
console.log(add(3,5))


// comece a criar a sua função multiply na linha abaixo

 function multiply(n1, n2){
   let resultado = 0
      
   for(let i = 0 ; i < n2 ; i++){
   
     resultado  +=  n1;
   }
   return resultado

   
 }

 console.log(multiply(4,6))



// descomente a linha seguinte para testar sua função
 console.assert(multiply(4, 6) === 24, 'A função multiply não está funcionando como esperado');


// comece a criar a sua função power na linha abaixo

function power(x, n){
  let resultado = 0
  
  for(let i = 1; i<= n; i++){
   
   resultado = resultado +  x * i

  }

  return resultado
  
}


// descomente a linha seguinte para testar sua função
console.log(power(3,4))
console.assert(power(3, 4) === 81, 'A função power não está funcionando como esperado');


// comece a criar a sua função factorial na linha abaixo

function factorial(n){
  let resultado = n
  for (let i = 1; i < n; i++){    
     resultado = resultado * (n - i)
         
  }
  return resultado
}



// descomente a linha seguinte para testar sua função
console.log(factorial(10))
console.assert(factorial(5) === 120, 'A função factorial não está funcionando como esperado');


/**
 * BONUS (aviso: o grau de dificuldade é bem maior !!!)
 */

// crie a função fibonacci

function fibonacci(n){

  let f1 =0
  let f2 =1
  let resultado = 0

if(n < 0){

  console.log('Número invalído')
}

while(f2 < n){

  resultado = f2 + f1

  f1 = f2
  f2 = resultado

}

return resultado;

  
}


console.log(fibonacci(8))


// descomente a linha seguinte para testar sua função
// console.assert(fibonacci(8) === 13, 'A função fibonacci não está funcionando como esperado');
